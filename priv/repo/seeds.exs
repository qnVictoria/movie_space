# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     MovieSpace.Repo.insert!(%MovieSpace.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias MovieSpace.Movies
alias MovieSpace.Actors
alias MovieSpace.Repo

movie1 = Movies.create_movie!(%{title: "Titanic", description: "disaster film", release_date: ~D[1997-12-19], genres: ["drama"], image: %Plug.Upload{content_type: "image/jpg", filename: "Titanic.jpg", path: "assets/static/images/Titanic.jpg"}})
movie2 = Movies.create_movie!(%{title: "Catch Me If You Can", description: "biographical crime film", release_date: ~D[2002-12-25], genres: ["detective", "tragicomedy"], image: %Plug.Upload{content_type: "image/jpg", filename: "Catch_Me_If_You_Can.jpg", path: "assets/static/images/Catch_Me_If_You_Can.jpg"}})
movie3 = Movies.create_movie!(%{title: "Once Upon a Time in Hollywood", description: "comedy-drama film", release_date: ~D[2019-05-21], genres: ["comedy", "drama"], image: %Plug.Upload{content_type: "image/jpg", filename: "Once_Upon_a_Time_in_Hollywood.jpg", path: "assets/static/images/Once_Upon_a_Time_in_Hollywood.jpg"}})
movie4 = Movies.create_movie!(%{title: "Mission Impossible", description: "action spy film", release_date: ~D[1996-05-22], genres: ["detective", "thriller"], image: %Plug.Upload{content_type: "image/jpg", filename: "Mission_Impossible.jpg", path: "assets/static/images/Mission_Impossible.jpg"}})
movie5 = Movies.create_movie!(%{title: "Suicide Squad", description: "superhero film", release_date: ~D[2016-08-01], genres: ["comedy"], image: %Plug.Upload{content_type: "image/jpg", filename: "Suicide_Squad.jpg", path: "assets/static/images/Suicide_Squad.jpg"}})

actor1 = Actors.create_actor(%{first_name: "Leonardo", last_name: "DiCaprio"})
actor2 = Actors.create_actor(%{first_name: "Kate", last_name: "Winslet"})
actor3 = Actors.create_actor(%{first_name: "Tom", last_name: "Hanks"})
actor4 = Actors.create_actor(%{first_name: "Brad", last_name: "Pitt"})
actor5 = Actors.create_actor(%{first_name: "Tom", last_name: "Cruise"})
actor6 = Actors.create_actor(%{first_name: "Margot", last_name: "Robbie"})

movie1
|> Repo.preload(:actors)
|> Ecto.Changeset.change
|> Ecto.Changeset.put_assoc(:actors, [actor1, actor2])
|> Repo.update!

movie2
|> Repo.preload(:actors)
|> Ecto.Changeset.change
|> Ecto.Changeset.put_assoc(:actors, [actor1, actor3])
|> Repo.update!

movie3
|> Repo.preload(:actors)
|> Ecto.Changeset.change
|> Ecto.Changeset.put_assoc(:actors, [actor1, actor4, actor6])
|> Repo.update!

movie4
|> Repo.preload(:actors)
|> Ecto.Changeset.change
|> Ecto.Changeset.put_assoc(:actors, [actor5])
|> Repo.update!

movie5
|> Repo.preload(:actors)
|> Ecto.Changeset.change
|> Ecto.Changeset.put_assoc(:actors, [actor6])
|> Repo.update!
