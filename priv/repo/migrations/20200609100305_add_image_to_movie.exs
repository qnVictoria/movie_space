defmodule MovieSpace.Repo.Migrations.AddImageToMovie do
  use Ecto.Migration

  def change do
    alter table(:movies) do
      add :image, :string
    end
  end
end
