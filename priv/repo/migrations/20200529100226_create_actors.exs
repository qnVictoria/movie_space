defmodule MovieSpace.Repo.Migrations.CreateActors do
  use Ecto.Migration

  def change do
    create table(:actors) do
      add :first_name, :string
      add :last_name, :string
    end
  end
end
