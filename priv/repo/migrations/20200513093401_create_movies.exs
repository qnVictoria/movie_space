defmodule MovieSpace.Repo.Migrations.CreateMovies do
  use Ecto.Migration

  def change do
    create table(:movies) do
      add :title, :string
      add :description, :string
      add :release_date, :date

      timestamps()
    end

  end
end
