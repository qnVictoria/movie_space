defmodule MovieSpace.Repo.Migrations.AddGenresToMovie do
  use Ecto.Migration

  def change do
    alter table(:movies) do
      add :genres, {:array, :string}
    end
  end
end
