defmodule MovieSpace.Repo.Migrations.UserCommentLikes do
  use Ecto.Migration

  def change do
    create table(:user_comment_likes) do
      add :comment_id, references(:comments, on_delete: :delete_all)
      add :user_id, references(:users, on_delete: :delete_all)
    end

    create unique_index(:user_comment_likes, [:user_id, :comment_id])
  end
end
