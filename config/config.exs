# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :movie_space,
  ecto_repos: [MovieSpace.Repo]

# Configures the endpoint
config :movie_space, MovieSpaceWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: System.get_env("SECRET_KEY_BASE"),
  render_errors: [view: MovieSpaceWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: MovieSpace.PubSub,
  live_view: [signing_salt: "U9IPE6Jg"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :movie_space, :pow,
  user: MovieSpace.Users.User,
  repo: MovieSpace.Repo,
  web_module: MovieSpaceWeb,
  extensions: [PowResetPassword],
  controller_callbacks: Pow.Extension.Phoenix.ControllerCallbacks,
  mailer_backend: MovieSpaceWeb.PowMailer

config :movie_space, :pow_assent,
  providers: [
    github: [
      client_id: System.get_env("GITHUB_ID"),
      client_secret: System.get_env("GITHUB_SECRET"),
      strategy: Assent.Strategy.Github
    ]
  ]

# Configures file uploading
config :arc,
  storage: Arc.Storage.Local

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
