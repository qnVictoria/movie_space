defmodule MovieSpace.Actors.Actor do
  use Ecto.Schema
  import Ecto.Changeset

  schema "actors" do
    field :first_name, :string
    field :last_name, :string
    many_to_many :movies, MovieSpace.Movies.Movie, join_through: "movies_actors"
  end

  def changeset(actor, attrs) do
    actor
    |> cast(attrs, [:first_name, :last_name])
    |> validate_required([:first_name, :last_name])
  end
end
