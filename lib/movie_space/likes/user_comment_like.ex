defmodule MovieSpace.Likes.UserCommentLike do
  use Ecto.Schema
  import Ecto.Changeset

  schema "user_comment_likes" do
    belongs_to :comment, MovieSpace.Comments.Comment, foreign_key: :comment_id
    belongs_to :user, MovieSpace.Users.User, foreign_key: :user_id
  end

  def changeset(like, attrs) do
    like
    |> cast(attrs, [:comment_id, :user_id])
    |> validate_required([:comment_id, :user_id])
    |> unique_constraint([:comment_id, :user_id])
  end
end
