defmodule MovieSpace.UserCommentLikes do
  @moduledoc """
  The likes context.
  """

  import Ecto.Query, warn: false
  alias MovieSpace.Repo

  alias MovieSpace.Likes.UserCommentLike
  alias MovieSpace.Comments

  @topic inspect(Comments)

  def get_like(comment_id, user_id), do: Repo.get_by(UserCommentLike, comment_id: comment_id, user_id: user_id)

  def create_like(attrs \\ %{}) do
    %UserCommentLike{}
    |> UserCommentLike.changeset(attrs)
    |> Repo.insert!
    |> broadcast_change([:comment, :liked])
  end

  def delete_like(%UserCommentLike{} = like) do
    like
    |> Repo.delete()
    |> broadcast_change([:comment, :unliked])
  end

  def subscribe do
    Phoenix.PubSub.subscribe(MovieSpace.PubSub, @topic)
  end

  defp broadcast_change(result, event) do
    Phoenix.PubSub.broadcast(MovieSpace.PubSub, @topic, {__MODULE__, event, result})

    {:ok, result}
  end
end
