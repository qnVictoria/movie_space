defmodule MovieSpace.Movies do
  @moduledoc """
  The Movies context.
  """

  import Ecto.Query, warn: false
  alias MovieSpace.Repo

  alias MovieSpace.Movies.Movie
  alias MovieSpace.Actors.Actor

  @doc """
  Returns the list of movies.

  ## Examples

      iex> list_movies()
      [%Movie{}, ...]

  """
  def list_movies(params) do
    search_term = get_in(params, ["query"])

    wildcard_search = "%#{search_term}%"

    search_query = from movie in Movie, distinct: true,
                   left_join: ma in "movies_actors", on: ma.movie_id == movie.id,
                   left_join: actor in Actor, on: actor.id == ma.actor_id,
                   where: ilike(movie.title, ^wildcard_search),
                   or_where: ilike(movie.description, ^wildcard_search),
                   or_where: ilike(actor.first_name, ^wildcard_search),
                   or_where: ilike(actor.last_name, ^wildcard_search)

    Repo.all(search_query)
  end

  @doc """
  Gets a single movie.

  Raises `Ecto.NoResultsError` if the Movie does not exist.

  ## Examples

      iex> get_movie!(123)
      %Movie{}

      iex> get_movie!(456)
      ** (Ecto.NoResultsError)

  """
  def get_movie!(id), do: Repo.get!(Movie, id)

  @doc """
  Creates a movie.

  ## Examples

      iex> create_movie(%{field: value})
      {:ok, %Movie{}}

      iex> create_movie(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_movie(attrs \\ %{}) do
    %Movie{}
    |> Movie.changeset(attrs)
    |> Repo.insert
  end

  def create_movie!(attrs \\ %{}) do
    %Movie{}
    |> Movie.changeset(attrs)
    |> Repo.insert!
  end

  @doc """
  Updates a movie.

  ## Examples

      iex> update_movie(movie, %{field: new_value})
      {:ok, %Movie{}}

      iex> update_movie(movie, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_movie(%Movie{} = movie, attrs) do
    movie
    |> Movie.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a movie.

  ## Examples

      iex> delete_movie(movie)
      {:ok, %Movie{}}

      iex> delete_movie(movie)
      {:error, %Ecto.Changeset{}}

  """
  def delete_movie(%Movie{} = movie) do
    Repo.delete(movie)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking movie changes.

  ## Examples

      iex> change_movie(movie)
      %Ecto.Changeset{data: %Movie{}}

  """
  def change_movie(%Movie{} = movie, attrs \\ %{}) do
    Movie.changeset(movie, attrs)
  end
end
