defmodule MovieSpace.Users.User do
  use Ecto.Schema
  use Pow.Ecto.Schema
  use PowAssent.Ecto.Schema
  use Pow.Extension.Ecto.Schema,
    extensions: [PowResetPassword]

  schema "users" do
    field :name, :string
    field :picture, :string

    has_many :comments, MovieSpace.Comments.Comment

    pow_user_fields()
    timestamps()
  end

  def changeset(user_or_changeset, attrs) do
    user_or_changeset
    |> pow_changeset(attrs)
    |> Ecto.Changeset.cast(attrs, [:name])
    |> pow_extension_changeset(attrs)
  end

  def user_identity_changeset(user_or_changeset, user_identity, attrs, user_id_attrs) do
    user_or_changeset
    |> Ecto.Changeset.cast(attrs, [:name, :picture])
    |> pow_assent_user_identity_changeset(user_identity, attrs, user_id_attrs)
  end

  # def avatar_url(user) do
  #   cond do
  #     user.picture != nil && user.picture != "" -> user.picture
  #     true -> "assets/static/images/default_user.jpg"
  #   end
  # end
end
