defmodule MovieSpace.Comments do

  import Ecto.Query, warn: false

  alias MovieSpace.Repo
  alias MovieSpace.Comments.Comment
  alias MovieSpace.Likes.UserCommentLike

  @topic inspect(__MODULE__)

  @doc """
  Returns the list of comments.

  """
  def list_comments(movie_id, user_id) do
    query = from comment in Comment,
            left_join: like in UserCommentLike, on: comment.id == like.comment_id and like.user_id == ^user_id,
            where: comment.movie_id == ^movie_id,
            order_by: [desc: comment.id],
            group_by: comment.id,
            select_merge: %{likes_count: count(like.id)}

    query
    |> Repo.all
    |> Repo.preload([:user, :likes])
  end

  def get_comment!(id), do: Repo.get!(Comment, id)

  @doc """
  Creates a comment.

  """
  def create_comment(attrs \\ %{}) do
    %Comment{}
    |> Comment.changeset(attrs)
    |> Repo.insert!
    |> broadcast_change([:comment, :created])
  end

  @doc """
  Deletes a comment.

  """
  def delete_comment(%Comment{} = comment) do
    comment
    |> Repo.delete()
    |> broadcast_change([:comment, :deleted])
  end

  def subscribe do
    Phoenix.PubSub.subscribe(MovieSpace.PubSub, @topic)
  end

  defp broadcast_change(result, event) do
    Phoenix.PubSub.broadcast(MovieSpace.PubSub, @topic, {__MODULE__, event, result})

    {:ok, result}
  end
end
