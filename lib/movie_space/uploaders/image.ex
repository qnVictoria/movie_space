defmodule MovieSpace.Image do
  use Arc.Definition
  use Arc.Ecto.Definition

  @versions [:original, :thumb]

  def validate({file, _}) do
    ~w(.jpg .jpeg .png) |> Enum.member?(Path.extname(file.file_name))
  end

  def transform(:thumb, _) do
    {:convert, "-strip -thumbnail 600^x300 -extent 600x300 -format png", :jpg}
  end

  def filename(version, _) do
    version
  end

  def storage_dir(_, {_, scope}) do
    folder_name = scope.title |> String.downcase |> String.replace(" ", "_")
    "uploads/movie/images/#{folder_name}"
  end

  def default_url(_version, _) do
    "assets/static/images/default_image.jpg"
  end
end
