defmodule MovieSpace.Comments.Comment do
  use Ecto.Schema
  import Ecto.Changeset

  schema "comments" do
    field :body, :string
    field :likes_count, :integer, virtual: true

    has_many :likes, MovieSpace.Likes.UserCommentLike

    belongs_to :movie, MovieSpace.Movies.Movie
    belongs_to :user, MovieSpace.Users.User, foreign_key: :user_id

    timestamps()
  end

  def changeset(comment, attrs) do
    comment
    |> cast(attrs, [:body, :user_id, :movie_id])
    |> validate_required([:body, :user_id, :movie_id])
  end

  def human_date(naive) do
    [naive.day, naive.month, naive.year]
    |> Enum.join("-")
  end
end
