defmodule MovieSpace.Repo do
  use Ecto.Repo,
    otp_app: :movie_space,
    adapter: Ecto.Adapters.Postgres
end
