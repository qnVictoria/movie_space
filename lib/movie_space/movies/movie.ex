defmodule MovieSpace.Movies.Movie do
  use Ecto.Schema
  use Arc.Ecto.Schema
  import Ecto.Changeset

  schema "movies" do
    field :title, :string
    field :description, :string
    field :release_date, :date
    field :image, MovieSpace.Image.Type
    field :genres, {:array, :string}
    many_to_many :actors, MovieSpace.Actors.Actor, join_through: "movies_actors"
    has_many :comments, MovieSpace.Comments.Comment

    timestamps()
  end

  @doc false
  def changeset(movie, attrs) do
    movie
    |> cast(attrs, [:title, :description, :release_date, :genres])
    |> cast_attachments(attrs, [:image])
    |> validate_required([:title, :description, :release_date, :genres, :image])
  end

  def genres_list(movie) do
    Enum.join(movie.genres, ", ")
  end
end
