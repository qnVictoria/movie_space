defmodule MovieSpaceWeb.PowResetPassword.MailerView do
  use MovieSpaceWeb, :mailer_view

  def subject(:reset_password, _assigns), do: "Reset password link"
end
