defmodule MovieSpaceWeb.CommentLive do
  use Phoenix.LiveView

  alias MovieSpace.Comments
  alias MovieSpace.UserCommentLikes
  alias MovieSpaceWeb.CommentView

  def mount(_params, %{"current_user_id" => current_user_id, "movie_id" => movie_id}, socket) do
    Comments.subscribe()

    {:ok, fetch(socket, current_user_id, movie_id)}
  end

  def render(assigns) do
    CommentView.render("comments.html", assigns)
  end

  def handle_event("add", %{"comment" => %{"body" => body}}, %{assigns: %{current_user_id: current_user_id, movie_id: movie_id}} = socket) do
    Comments.create_comment(%{body: body, user_id: current_user_id, movie_id: movie_id})

    {:noreply, fetch(socket, current_user_id, movie_id)}
  end

  def handle_event("like", %{"comment-id" => id}, %{assigns: %{current_user_id: current_user_id, movie_id: movie_id}} = socket) do
    {comment_id, _} = Integer.parse(id)

    UserCommentLikes.create_like(%{comment_id: comment_id, user_id: current_user_id})

    {:noreply, fetch(socket, current_user_id, movie_id)}
  end

  def handle_event("unlike", %{"comment-id" => id}, %{assigns: %{current_user_id: current_user_id, movie_id: movie_id}} = socket) do
    {comment_id, _} = Integer.parse(id)

    comment_id
    |> UserCommentLikes.get_like(current_user_id)
    |> UserCommentLikes.delete_like

    {:noreply, fetch(socket, current_user_id, movie_id)}
  end

  def handle_event("delete", %{"comment-id" => id}, %{assigns: %{current_user_id: current_user_id, movie_id: movie_id}} = socket) do
    {comment_id, _} = Integer.parse(id)

    comment_id
    |> Comments.get_comment!
    |> Comments.delete_comment

    {:noreply, fetch(socket, current_user_id, movie_id)}
  end

  def handle_info({Comments, [:comment | _], _}, %{assigns: %{current_user_id: current_user_id, movie_id: movie_id}} = socket) do
    {:noreply, fetch(socket, current_user_id, movie_id)}
  end

  def handle_info({UserCommentLikes, [:comment | _], _}, %{assigns: %{current_user_id: current_user_id, movie_id: movie_id}} = socket) do
    {:noreply, fetch(socket, current_user_id, movie_id)}
  end

  defp fetch(socket, current_user_id, movie_id) do
    assign(socket, comments: Comments.list_comments(movie_id, current_user_id), current_user_id: current_user_id, movie_id: movie_id)
  end
end
