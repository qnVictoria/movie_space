defmodule MovieSpaceWeb.MovieController do
  use MovieSpaceWeb, :controller

  alias MovieSpace.Movies
  alias MovieSpace.Movies.Movie

  def index(conn, params) do
    movies = Movies.list_movies(params)
    render(conn, "index.html", movies: movies)
  end

  def show(conn, %{"id" => id}) do
    movie = Movies.get_movie!(id)
    render(conn, "show.html", movie: movie)
  end

  def new(conn, params) do
    changeset = Movie.changeset(%Movie{}, params)
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"movie" => movie_params}) do
    case Movies.create_movie(movie_params) do
      {:ok, movie} ->
        conn
        |> put_flash(:info, "Movie created successfully.")
        |> redirect(to: Routes.movie_path(conn, :show, movie.id))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end
end
