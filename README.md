# MovieSpace

How to run a project

## Build

`docker-compose build`

## Install assets

`docker-compose run app npm install --prefix assets`

## Run server

`docker-compose up`

## Seeding db

`docker-compose run app mix run priv/repo/seeds.exs`

## Open website

[`localhost:4000`](http://localhost:4000)
